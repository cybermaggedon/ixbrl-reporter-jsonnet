[
    {
	"id": "rnd",
	"kind": "simple",
	"description": "SME R&D",
	"computations": [
	    "rnd-enhanced-expenditure"
	]
    },
    {
	"id": "tax-calculation",
	"kind": "simple",
	"description": "Tax Calculation",
	"computations": [
	    "ct-trading-profits-raw",
	    "ct-net-trading-profits",
	    "ct-trading-losses",
	    "ct-profit-before-tax-total",
	    "ct-tax-total"
	]
    },
    {
	"id": "detailed-profit-and-loss",
	"kind": "simple",
	"description": "Detailed Profit-and-Loss",
	"computations": [
	    "turnover",
	    "gross-profit",
	    "total-costs",
	    "profit-before-tax",
	    "tax-due",
	    "profit"
	]
    }
]
